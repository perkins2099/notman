require 'spec_helper'

describe "locations/index" do
  before(:each) do
    assign(:locations, [
      stub_model(Location,
        :name => "Name",
        :reely_receiver_id => "Reely Receiver"
      ),
      stub_model(Location,
        :name => "Name",
        :reely_receiver_id => "Reely Receiver"
      )
    ])
  end

  it "renders a list of locations" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Reely Receiver".to_s, :count => 2
  end
end
