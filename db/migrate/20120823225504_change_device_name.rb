class ChangeDeviceName < ActiveRecord::Migration
  def change
  	rename_column :users, :reely_receiver_id, :device_id
  	rename_column :locations, :reely_receiver_id, :device_id
  end

end
