class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.date :the_date
      t.integer :location_id
      t.integer :user_id

      t.timestamps
    end
  end
end
