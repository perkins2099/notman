class Location < ActiveRecord::Base
  attr_accessible :name, :device_id, :id

  has_many :events
  has_many :users, :through => :events
end
