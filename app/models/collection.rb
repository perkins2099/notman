class Collection
    def update_data


	require 'net/http'
    User.where(:device_id != nil).each do |user|

      uri = URI.parse("http://api.reelyactive.com/devices/#{user.device_id.to_s}/location")
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Get.new(uri.request_uri)
      request["Accept"] = "application/json"
      response = http.request(request)
      json = JSON.parse(response.body)

      #Rails.logger.info(json.to_yaml)
      #Rails.logger.info("---------------")
      location_device_id = json['Device']['Location']['Decoders']['Decoder']['ID']
      user_device_id = json['Device']['ID']
      the_date = json['Device']['TimeStamp']

      user = User.find_by_device_id(user_device_id)
      location = Location.find_by_device_id(location_device_id)

      if user and location and the_date
        Event.create(location_id:location.id, user_id: user.id, the_date:the_date)
      else
        Rails.logger.info("Failure----------->" + json.to_yaml)
      end
	end

        self.delay(:run_at => 1.minute.from_now).update_data
    end

    def self.start_me_up
        new.update_data
    end
end