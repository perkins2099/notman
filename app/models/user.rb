class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :id,
  	:first_name, :last_name, :nick_name, :team_id, :device_id
  # attr_accessible :title, :body

  belongs_to :team
  has_many :events
  has_many :locations, :through => :events

end
